# Caipirinha cocktail

## Ingredients

1. 5 cl cachaça
2. Half a lime cut into 4 wedges
3. 2 teaspoons sugar

## Final result

![Caipirinha](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/15-09-26-RalfR-WLC-0048.jpg/800px-15-09-26-RalfR-WLC-0048.jpg)
